package io.gitlab.arturbosch.detekt.rules.style

import io.gitlab.arturbosch.detekt.rules.setupKotlinEnvironment
import io.gitlab.arturbosch.detekt.test.assertThat
import io.gitlab.arturbosch.detekt.test.compileAndLintWithContext
import org.jetbrains.kotlin.cli.jvm.compiler.KotlinCoreEnvironment
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class InsecureDeserializationRuleSpec : Spek({
    setupKotlinEnvironment()

    val env: KotlinCoreEnvironment by memoized()

    describe("InsecureDeserialization rule") {

            it("TEST InsecureDeserialization rule") {
                val code = """
                    package com.example.demo
                    
                    import Command
                    import DefaultObject
                    import InfectedObject
                    import org.springframework.web.bind.annotation.GetMapping
                    import org.springframework.web.bind.annotation.PostMapping
                    import org.springframework.web.bind.annotation.RequestMapping
                    import org.springframework.web.bind.annotation.RestController
                    import java.io.*
                    
                    @RestController
                    @RequestMapping(path = ["/main"])
                    class MyController {
                    
                        @GetMapping
                        fun doGet() {
                    
                            try {
                                ObjectOutputStream(FileOutputStream("InfectedObject.ser")).use { outputStream -> outputStream.writeObject(InfectedObject(Command("netstat"))) }
                                
                            } catch (e: IOException) {
                                e.printStackTrace()
                            }
                        }
                    
                        @PostMapping
                        fun doPost(inputStream: InputStream?) {
                    
                            try {
                                val objectInputStream = ObjectInputStream(inputStream)
                                val deserializedObject: DefaultObject = objectInputStream.readObject() as DefaultObject
                    
                                System.out.println(deserializedObject)
                    
                            } catch (e: IOException) {
                                e.printStackTrace()
                            } catch (e: ClassNotFoundException) {
                                e.printStackTrace()
                            }
                        }
                    }
                """
                val findings = InsecureDeserializationRule(
                ).compileAndLintWithContext(env, code)
                assertThat(findings).hasSize(1)
            }
    }
})
