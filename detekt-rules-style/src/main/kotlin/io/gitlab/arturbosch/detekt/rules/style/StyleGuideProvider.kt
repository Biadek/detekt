package io.gitlab.arturbosch.detekt.rules.style

import io.gitlab.arturbosch.detekt.api.Config
import io.gitlab.arturbosch.detekt.api.RuleSet
import io.gitlab.arturbosch.detekt.api.internal.DefaultRuleSetProvider

/**
 * The Style ruleset provides rules that assert the style of the code.
 * This will help keep code in line with the given
 * code style guidelines.
 *
 * @active since v1.0.0
 */
class StyleGuideProvider : DefaultRuleSetProvider {

    override val ruleSetId: String = "style"

    @Suppress("LongMethod")
    override fun instance(config: Config): RuleSet = RuleSet(
        ruleSetId,
        listOf(
            InsecureDeserializationRule()
        )
    )
}
