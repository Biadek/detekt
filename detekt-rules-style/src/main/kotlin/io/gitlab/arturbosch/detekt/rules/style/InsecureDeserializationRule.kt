package io.gitlab.arturbosch.detekt.rules.style

import io.gitlab.arturbosch.detekt.api.*
import org.jetbrains.kotlin.psi.*
import org.jetbrains.kotlin.resolve.BindingContext
import org.jetbrains.kotlin.resolve.calls.callUtil.getCall

class InsecureDeserializationRule : Rule() {

    override val issue = Issue(
        javaClass.simpleName,
        Severity.Security,
        "Mark deserialization process call.",
        Debt.TEN_MINS
    )

    var namesOfObjectInputStream : MutableCollection<String?> = arrayListOf()

    override fun visitCallExpression(expression: KtCallExpression) {
        super.visitCallExpression(expression)
        checkCall(expression)
    }

    override fun visitNamedDeclaration(declaration: KtNamedDeclaration) {
        if (declaration.nameAsSafeName.isSpecial) {
            return
        }
        declaration.nameIdentifier?.parent?.javaClass?.let {
            when (declaration) {
                is KtClass -> handleClass()
                is KtProperty -> handleProperty(declaration)
                is KtParameter -> handleParameter(declaration)
                else -> declaration
            }
        }
        super.visitNamedDeclaration(declaration)
    }

    private fun handleClass() {
       namesOfObjectInputStream.clear()
    }

    private fun handleParameter(declaration: KtParameter): Any {
        val lastChild = declaration.lastChild

        if (lastChild.firstChild != null && lastChild.firstChild.firstChild != null) {
            val objectClass = lastChild.firstChild.firstChild.text
            val objectName = declaration.firstChild.text

            checkIfItsObjectInputStreamObject(objectClass, objectName)
        }

        return declaration
    }

    private fun handleProperty(declaration: KtProperty): Any {
        val objectClass = declaration.delegateExpressionOrInitializer?.firstChild?.text
        val objectName = declaration.identifyingElement?.text
        val propertyType = declaration.children[0].text.replace("?", EMPTY)

        checkIfItsObjectInputStreamObject(objectClass, objectName, propertyType)

        return declaration
    }

    private fun checkIfItsObjectInputStreamObject(objectClass: String?, objectName: String?, propertyType: String = EMPTY) {
        if (objectClass == OBJECT_INPUT_STREAM || propertyType == OBJECT_INPUT_STREAM) {
            namesOfObjectInputStream.add(objectName)
        }
    }

    private fun checkCall(expression: KtExpression) {
        if (bindingContext == BindingContext.EMPTY) return

        val call = expression.getCall(bindingContext) ?: return
        val callableElement = call.callElement.text
        val callerElement = call.callOperationNode?.treePrev?.firstChildNode?.text

        if (METHOD_READ_OBJECT == callableElement && isCalledByObjectInputStream(callerElement)) {
            report(
                    CodeSmell(
                            issue,
                            Entity.from(expression),
                            "You are trying deserialize the object." +
                                    " It's a potentially dangerous operation!"
                    )
            )
        }
    }

    private fun isCalledByObjectInputStream(callerElement: String?) =
            (namesOfObjectInputStream.contains(callerElement) || callerElement == IT_EXPRESSION || callerElement == OBJECT_INPUT_STREAM)

    companion object {
        const val METHOD_READ_OBJECT = "readObject()"
        const val OBJECT_INPUT_STREAM = "ObjectInputStream"
        const val IT_EXPRESSION = "it"
        const val EMPTY = ""
    }
}
