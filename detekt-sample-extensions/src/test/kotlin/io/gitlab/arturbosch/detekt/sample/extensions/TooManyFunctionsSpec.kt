package io.gitlab.arturbosch.detekt.sample.extensions

import io.gitlab.arturbosch.detekt.sample.extensions.rules.TooManyFunctions
import io.gitlab.arturbosch.detekt.test.lint
import org.assertj.core.api.Assertions.assertThat
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class TooManyFunctionsSpec : Spek({

    val subject by memoized { TooManyFunctions() }

    describe("a simple test") {

        it("TESTTEST") {
            val findings = subject.lint(code)
            assertThat(findings).hasSize(1)
        }
    }
})

const val code: String =
        """
        fun doPost(request: HttpServletRequest, response: HttpServletResponse?) {
                
                        val sis: ServletInputStream = request.inputStream
                        val ois = ObjectInputStream(sis)
                        try {
                
                            val obj: DefaultObject = ois.readObject() as DefaultObject
                            DriverManager.println(obj.toString())
                
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
        """







//const val code: String =
//    """
//        @RestController
//        @RequestMapping(path = ["/main"])
//        class MyController {
//
//
//            @PostMapping
//            fun doPost(inputStream: InputStream?) {
//
//                try {
//                    val objectInputStream = ObjectInputStream(inputStream)
//                    val deserializedObject: DefaultObject = objectInputStream.readObject() as DefaultObject
//
//                    System.out.println(deserializedObject)
//
//                } catch (e: IOException) {
//                    e.printStackTrace()
//                } catch (e: ClassNotFoundException) {
//                    e.printStackTrace()
//                }
//            }
//
//            fun deserializeAsDefaultObject(fileName: String) {
//
//                val inputStream = ObjectInputStream(FileInputStream(fileName))
//                val deserializedObject = inputStream.readObject() as DefaultObject
//                println(deserializedObject)
//
//                inputStream.close()
//            }
//
//            fun doPost(request: HttpServletRequest, response: HttpServletResponse?) {
//
//                val sis: ServletInputStream = request.inputStream
//                val ois = ObjectInputStream(sis)
//                try {
//
//                    val obj: DefaultObject = ois.readObject() as DefaultObject
//                    DriverManager.println(obj.toString())
//
//                } catch (e: Exception) {
//                    e.printStackTrace()
//                }
//            }
//        }
//
//        """
