package io.gitlab.arturbosch.detekt.sample.extensions.rules

import io.gitlab.arturbosch.detekt.api.CodeSmell
import io.gitlab.arturbosch.detekt.api.Debt
import io.gitlab.arturbosch.detekt.api.Entity
import io.gitlab.arturbosch.detekt.api.Issue
import io.gitlab.arturbosch.detekt.api.Rule
import io.gitlab.arturbosch.detekt.api.Severity
import io.gitlab.arturbosch.detekt.api.internal.valueOrDefaultCommaSeparated
import io.gitlab.arturbosch.detekt.rules.extractMethodNameAndParams
import org.jetbrains.kotlin.js.descriptorUtils.getJetTypeFqName
import org.jetbrains.kotlin.psi.*
import org.jetbrains.kotlin.resolve.BindingContext
import org.jetbrains.kotlin.resolve.calls.callUtil.getResolvedCall
import org.jetbrains.kotlin.resolve.descriptorUtil.fqNameOrNull


class TooManyFunctions : Rule() {

    override val issue = Issue(
        javaClass.simpleName,
        Severity.Security,
        "This rule reports a insecure deserialization",
        Debt.TWENTY_MINS
    )

    private val forbiddenMethods = valueOrDefaultCommaSeparated("java.io.ObjectInputStream.readObject", listOf())
            .map { extractMethodNameAndParams(it) }



    override fun visitCallExpression(expression: KtCallExpression) {
        super.visitCallExpression(expression)
        check(expression)
    }

    override fun visitBinaryExpression(expression: KtBinaryExpression) {
        super.visitBinaryExpression(expression)
        check(expression.operationReference)
    }

    override fun visitPrefixExpression(expression: KtPrefixExpression) {
        super.visitPrefixExpression(expression)
        check(expression.operationReference)
    }

    override fun visitPostfixExpression(expression: KtPostfixExpression) {
        super.visitPostfixExpression(expression)
        check(expression.operationReference)
    }

    private fun check(expression: KtExpression) {
        if (bindingContext == BindingContext.EMPTY) return

        val resolvedCall = expression.getResolvedCall(bindingContext) ?: return
        val methodName = resolvedCall.resultingDescriptor.fqNameOrNull()?.asString()
        val encounteredParamTypes = resolvedCall.resultingDescriptor.valueParameters
                .map { it.type.getJetTypeFqName(false) }

        if (methodName != null) {
            forbiddenMethods
                    .filter { methodName == it.first }
                    .forEach {
                        val expectedParamTypes = it.second
                        val noParamsProvided = expectedParamTypes == null
                        val paramsMatch = expectedParamTypes == encounteredParamTypes

                        if (noParamsProvided || paramsMatch) {
                            report(
                                    CodeSmell(
                                            issue,
                                            Entity.from(expression),
                                            "The method ${it.first}(${expectedParamTypes?.joinToString() ?: ""}) " +
                                                    "has been forbidden in the Detekt config."
                                    )
                            )
                        }
                    }
        }
    }











//    private var amount: Int = 0
//
//    override fun visitKtFile(file: KtFile) {
//        super.visitKtFile(file)
//        if (amount > THRESHOLD) {
//            report(CodeSmell(issue, Entity.atPackageOrFirstDecl(file),
//                message = "The file ${file.name} has $amount function declarations. " +
//                    "Threshold is specified with $THRESHOLD."))
//        }
//        amount = 0
//    }
//
//    override fun visitNamedFunction(function: KtNamedFunction) {
//        super.visitNamedFunction(function)
//        amount++
//    }
}

const val THRESHOLD = 10
